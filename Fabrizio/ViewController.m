//
//  ViewController.m
//  Fabrizio
//
//  Created by Eric Berna on 5/4/12.
//  Copyright (c) 2012 CI Design Inc. All rights reserved.
//

#import "ViewController.h"
#import "OneViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize navController;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	OneViewController *oneVC = [[OneViewController alloc] initWithNibName:@"OneViewController" bundle:nil];
	self.navController = [[UINavigationController alloc]initWithRootViewController:oneVC];
	self.navController.view.frame = CGRectMake(0.0, 64.0, 320.0, 416.0);
	[self.view addSubview:self.navController.view];
	[self addChildViewController:self.navController];
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
