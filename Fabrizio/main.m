//
//  main.m
//  Fabrizio
//
//  Created by Eric Berna on 5/4/12.
//  Copyright (c) 2012 CI Design Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
