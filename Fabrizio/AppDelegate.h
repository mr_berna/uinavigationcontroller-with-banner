//
//  AppDelegate.h
//  Fabrizio
//
//  Created by Eric Berna on 5/4/12.
//  Copyright (c) 2012 CI Design Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
