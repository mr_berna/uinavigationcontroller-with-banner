//
//  ViewController.h
//  Fabrizio
//
//  Created by Eric Berna on 5/4/12.
//  Copyright (c) 2012 CI Design Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) UINavigationController *navController;

@end
